package com.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.model.Empleado;

public interface IEmpleadoDao extends MongoRepository<Empleado, String> {

}
