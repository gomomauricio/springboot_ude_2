package com.services;

import java.util.List;

import com.model.Empleado;

public interface IEmpleadosService {
	
	List<Empleado> buscar();
	Empleado buscarPorId(String id);
	Empleado guardar(Empleado empleado);
	void eliminar(String id);

}
