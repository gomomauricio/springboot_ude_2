package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import com.dao.ICategoriaDao;
import com.model.Categorias;
import com.response.CategoriaResponseRest;
import com.service.impl.CategoriaServiceImpl;

public class CategoriaServiceImplTest {
	
	@InjectMocks
	CategoriaServiceImpl service;
	
	@Mock
	ICategoriaDao categoriaDao;
	
	List<Categorias> lsCat = new ArrayList<>();
	
	@BeforeEach
	public void init()
	{
		MockitoAnnotations.openMocks(this);
		this.cargarCategorias();
	}
	
	@Test
	public void buscarCategoriasTest()
	{
		when( categoriaDao.findAll() ).thenReturn(lsCat);
		
		ResponseEntity<CategoriaResponseRest> response = service.buscarCategorias();
		
		assertEquals(4, response.getBody().getCategoriaResponse().getCategoria().size());
		
		verify( categoriaDao, times(1)).findAll(); //si se mando a ejecutar la clase categoria Dao solo una vez
		
	}
	
	
	public void cargarCategorias()
	{
		Categorias cat1 = new Categorias(Long.valueOf(1), "Abarrotes", "Distintos Abarrotes");
		lsCat.add(cat1);
		Categorias cat2 = new Categorias(Long.valueOf(2), "Lacteos", "variedad de quesos");
		lsCat.add(cat2);
		Categorias cat3 = new Categorias(Long.valueOf(3), "Bebidas", "Bebidas sin azucar");
		lsCat.add(cat3);
		Categorias cat4 = new Categorias(Long.valueOf(4), "Carnes blancas", "Distintas Carnes");
		lsCat.add(cat4);
	}
	

}
