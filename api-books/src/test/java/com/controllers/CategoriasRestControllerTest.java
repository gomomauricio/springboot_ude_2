package com.controllers;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.controller.CategoriaRestController;
import com.model.Categorias;
import com.response.CategoriaResponseRest;
import com.service.ICategoriaService;

public class CategoriasRestControllerTest {
	@InjectMocks
	CategoriaRestController categoriaController;

	@Mock
	ICategoriaService service;

	@BeforeEach // por cada prueba que se ejecute del test se inicia este metodo antes
	public void init() {
		MockitoAnnotations.openMocks(this);

	}

	@Test
	public void crearTest() {
		MockHttpServletRequest request = new MockHttpServletRequest();

		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		Categorias categoria = new Categorias(Long.valueOf(1), "Clasicos", "Libros clásicos de literatura");

		when(service.crear(any(Categorias.class))).thenReturn(new ResponseEntity<CategoriaResponseRest>(HttpStatus.OK));

		ResponseEntity<CategoriaResponseRest> respuesta = categoriaController.crear(categoria);
		
		assertThat( respuesta.getStatusCodeValue()).isEqualTo(200);
		
	}

}
