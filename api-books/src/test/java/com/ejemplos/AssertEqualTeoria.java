package com.ejemplos;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AssertEqualTeoria {
	
	@Test
	@DisplayName("Prueba de Teoria")
	public void miTest()
	{
		assertEquals(1, 1);
		assertNotEquals(1, 2);
		assertTrue(true);
		assertFalse(false);
		
		boolean expresion1 = 4 == 4;
		assertTrue(expresion1);
		
		String[] arre1 = {"a","b"};
		String[] arre2 = {"a","b"};
		String[] arre3 = {"a","b","c"};
		
		assertArrayEquals(arre1, arre2);
	}

}
