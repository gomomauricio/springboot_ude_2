package com.dao;

import org.springframework.data.repository.CrudRepository;

import com.model.Libro;


public interface ILibroDao extends CrudRepository<Libro, Long>{

}
