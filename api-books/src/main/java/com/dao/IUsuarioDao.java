package com.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.model.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long> {

	public Usuario findByNombreUsuario(String nombreUsuario); //findBy y nombre de la propiedad que esta en nuestra clase
	
	@Query("select u from Usuario u where u.nombreUsuario = ?1" )  //le pasamos el parametro 
	public Usuario findByIdNombreUsuarioV2(String nombreUsuario);
	
}
