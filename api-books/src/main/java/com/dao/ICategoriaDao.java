package com.dao;

import org.springframework.data.repository.CrudRepository;

import com.model.Categorias;

public interface ICategoriaDao extends CrudRepository<Categorias, Long>{

}
