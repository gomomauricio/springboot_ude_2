package com.response;

import java.util.List;

import com.model.Categorias;

public class CategoriaResponse 
{
	private List<Categorias> categoria;

	public List<Categorias> getCategoria() {
		return categoria;
	}

	public void setCategoria(List<Categorias> categoria) {
		this.categoria = categoria;
	}
	
	

}
