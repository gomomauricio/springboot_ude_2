package com.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.IUsuarioDao;
import com.model.Usuario;

@Service //crea un servicio para implementar acceso a datos
public class UsuarioService implements UserDetailsService{
	
	private static final Logger log = LoggerFactory.getLogger( UsuarioService.class );
	
	@Autowired //inyeccion de dependencias
	private IUsuarioDao usuarioDao;

	@Override
	@Transactional(readOnly = true) //solo lectura
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Usuario usuario = this.usuarioDao.findByNombreUsuario(username);
		
		if(usuario == null)
		{
			log.error(" Error, el usuario no existe ");
			throw new UsernameNotFoundException(" Error, el usuario no existe ");
		}
		
		//devuelve una lista de roles
		List<GrantedAuthority> authorities = usuario.getRoles()
													.stream()
													.map( rol -> new SimpleGrantedAuthority( rol.getNombre() ))
													.peek( authority -> log.info("Rol: " + authority.getAuthority()))
													.collect( Collectors.toList() );
				

		
		
		return new User( usuario.getNombreUsuario(), 
				         usuario.getPassword(),
				         usuario.getHabilitado(), 
				         true, 
				         true, 
				         true, 
				         authorities);
	}

}
