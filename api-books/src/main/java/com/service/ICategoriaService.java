package com.service;

import org.springframework.http.ResponseEntity;

import com.model.Categorias;
import com.response.CategoriaResponseRest;

public interface ICategoriaService {
	
	public ResponseEntity<CategoriaResponseRest> buscarCategorias();
	public ResponseEntity<CategoriaResponseRest> buscarPorId(Long id);
	public ResponseEntity<CategoriaResponseRest> crear(Categorias categoria);
	public ResponseEntity<CategoriaResponseRest> actualizar(Categorias categoria,Long id);
	public ResponseEntity<CategoriaResponseRest> eliminar( Long id );
	

}
