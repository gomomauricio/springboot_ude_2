package com.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.ICategoriaDao;
import com.model.Categorias;
import com.response.CategoriaResponseRest;
import com.service.ICategoriaService;

@Service //se encargara de acceder a la base
public class CategoriaServiceImpl implements ICategoriaService 
{
	
	private static final Logger log = LoggerFactory.getLogger( CategoriaServiceImpl.class);
	
	@Autowired
	private ICategoriaDao categoriaDAO;

	@Override
	@Transactional(readOnly = true) //hace la transaccion por nosotros
	public ResponseEntity<CategoriaResponseRest> buscarCategorias() {
		// TODO Auto-generated method stub
		log.info("Inicio de metodo buscarCategorias() ");
		
		CategoriaResponseRest response = new CategoriaResponseRest();
		
		try {
			
			List<Categorias> categoria = (List<Categorias>) categoriaDAO.findAll();
			
			response.getCategoriaResponse().setCategoria(categoria);
			
			response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
			
			
		} catch (Exception e) {
			response.setMetadata("Respuesta nok", "-1", "Respuesta incorrecta");
			log.error( "Error en buscarCategorias() " , e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.OK);
	}
	
	

	@Override
	@Transactional(readOnly = true)  
	public ResponseEntity<CategoriaResponseRest> buscarPorId(Long id) {
	log.info("Inicio de metodo buscarCategorias() ");
		
		CategoriaResponseRest response = new CategoriaResponseRest();
		
		try {
			List<Categorias> lista = new ArrayList<>();
			Optional<Categorias> op = categoriaDAO.findById(id);
			
			if(op.isPresent())
			{
				lista.add(op.get());
				response.getCategoriaResponse().setCategoria(lista);
				response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
			}
			else
			{
				log.error( "Error en buscar por id " );
				response.setMetadata("Respuesta nok", "-1", "Id no encontrado  ");
				return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.NOT_FOUND);
			}
			
		} catch (Exception e) {
			
			response.setMetadata("Respuesta nok", "-1", "Error al consultar por id");
			log.error( "Error en buscar por id  " , e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.OK);
	}



	@Override
	@Transactional
	public ResponseEntity<CategoriaResponseRest> crear(Categorias categoria) {
			log.info("Inicio de metodo crear() ");
		
		CategoriaResponseRest response = new CategoriaResponseRest();
		
		try {
			List<Categorias> lista = new ArrayList<>();
			
			Categorias categoriaGuardada = categoriaDAO.save(categoria);
			
			if( categoriaGuardada != null)
			{
				lista.add( categoriaGuardada );
				response.getCategoriaResponse().setCategoria(lista);
			}
			else
			{
				log.error( "Error en guardar categoria " );
				response.setMetadata("Respuesta nok", "-1", "Categoria NO guardada");
				return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.BAD_REQUEST);
			}


			
			response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
			
			
		} catch (Exception e) {
			response.setMetadata("Respuesta nok", "-1", "Error al crear categoria" );
			log.error( "Error en crear categoria  " , e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		
		
		return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.OK);
	}



	@Override
	@Transactional
	public ResponseEntity<CategoriaResponseRest> actualizar(Categorias categoria, Long id) {
	log.info("Inicio de metodo actualizar() ");
		
		CategoriaResponseRest response = new CategoriaResponseRest();
		
		try {
			List<Categorias> lista = new ArrayList<>();
			
			Optional<Categorias> categoriaBuscada = categoriaDAO.findById(id);
			
			if(categoriaBuscada.isPresent())
			{
				categoriaBuscada.get().setNombre( categoria.getNombre() );
				categoriaBuscada.get().setDescripcion( categoria.getDescripcion() );
				
				Categorias categoriaActualizar = categoriaDAO.save( categoriaBuscada.get() );
				
				if( categoriaActualizar != null)
				{
					lista.add( categoriaActualizar );
					response.getCategoriaResponse().setCategoria(lista);
					response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
				}
				else
				{
					log.error( "Error en guardar categoria " );
					response.setMetadata("Respuesta nok", "-1", "Categoria NO actualizada");
					return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.BAD_REQUEST);
				}

			}
			else
			{
				log.error( "Error en guardar categoria " );
				response.setMetadata("Respuesta nok", "-1", "Categoria NO actualizada");
				return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.NOT_FOUND);
			}
			
			
			
		 
			

			
			
			
			
		} catch (Exception e) {
			response.setMetadata("Respuesta nok", "-1", "Error al actualizar categoria" );
			log.error( "Error en actualizar categoria  " , e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		
		
		return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.OK);
	}



	@Override
	@Transactional
	public ResponseEntity<CategoriaResponseRest> eliminar(Long id) 
	{
		log.info("Inicio de metodo eliminar() ");
		
		CategoriaResponseRest response = new CategoriaResponseRest();
		
		try {
			
			List<Categorias> lista = new ArrayList<>();
			
			Optional<Categorias> categoriaEliminar = categoriaDAO.findById(id);
			
			if(categoriaEliminar.isPresent())
			{
				  categoriaDAO.deleteById(id);
				 
					lista.add( categoriaEliminar.get() );
					response.setMetadata("Respuesta OK", "200", "Categoria Eliminada" );
					response.getCategoriaResponse().setCategoria(lista);
			 
			}
			else
			{
				log.error( "Error en guardar categoria " );
				response.setMetadata("Respuesta nok", "-1", "Categoria NO encontrada, no eliminada");
				return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.NOT_FOUND);
			} 
			
			
		} catch (Exception e) {
			response.setMetadata("Respuesta nok", "-1", "Error al eliminar categoria" );
			log.error( "Error en eliminar categoria  " , e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		
		
		return new ResponseEntity<CategoriaResponseRest>(response,HttpStatus.OK);
	}
	

}
