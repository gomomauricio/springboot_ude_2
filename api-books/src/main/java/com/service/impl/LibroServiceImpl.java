package com.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.ILibroDao;
import com.model.Libro;
import com.response.LibroResponseRest;
import com.service.ILibroService;

 

@Service //se encargara de acceder a la base
public class LibroServiceImpl implements ILibroService 
{
	
	private static final Logger log = LoggerFactory.getLogger( LibroServiceImpl.class);
	
	@Autowired
	private ILibroDao libroDAO;

	@Override
	@Transactional(readOnly = true) //hace la transaccion por nosotros
	public ResponseEntity<LibroResponseRest> buscarLibros() {
		// TODO Auto-generated method stub
		log.info("Inicio de metodo buscarLibros() ");
		
		LibroResponseRest response = new LibroResponseRest();
		
		try {
			
			List<Libro> Libro = (List<Libro>) libroDAO.findAll();
			
			response.getLibroResponse().setLibros(Libro);
			
			response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
			
			
		} catch (Exception e) {
			response.setMetadata("Respuesta nok", "-1", "Respuesta incorrecta");
			log.error( "Error en buscarLibros() " , e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<LibroResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<LibroResponseRest>(response,HttpStatus.OK);
	}
	
	 
	@Override
	@Transactional(readOnly = true)  
	public ResponseEntity<LibroResponseRest> buscarPorId(Long id) {
	log.info("Inicio de metodo buscarLibros() ");
		
		LibroResponseRest response = new LibroResponseRest();
		
		try {
			List<Libro> lista = new ArrayList<>();
			Optional<Libro> op = libroDAO.findById(id);
			
			if(op.isPresent())
			{
				lista.add(op.get());
				response.getLibroResponse().setLibros(lista);
				response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
			}
			else
			{
				log.error( "Error en buscar por id " );
				response.setMetadata("Respuesta nok", "-1", "Id no encontrado  ");
				return new ResponseEntity<LibroResponseRest>(response,HttpStatus.NOT_FOUND);
			}
			
		} catch (Exception e) {
			
			response.setMetadata("Respuesta nok", "-1", "Error al consultar por id");
			log.error( "Error en buscar por id  " , e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<LibroResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		return new ResponseEntity<LibroResponseRest>(response,HttpStatus.OK);
	}



	@Override
	@Transactional
	public ResponseEntity<LibroResponseRest> crear(Libro Libro) {
			log.info("Inicio de metodo crear() ");
		
		LibroResponseRest response = new LibroResponseRest();
		
		try {
			List<Libro> lista = new ArrayList<>();
			
			Libro LibroGuardado = libroDAO.save(Libro);
			
			if( LibroGuardado != null)
			{
				lista.add( LibroGuardado );
				response.getLibroResponse().setLibros(lista);
			}
			else
			{
				log.error( "Error en guardar Libro " );
				response.setMetadata("Respuesta nok", "-1", "Libro NO guardada");
				return new ResponseEntity<LibroResponseRest>(response,HttpStatus.BAD_REQUEST);
			}


			
			response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
			
			
		} catch (Exception e) {
			response.setMetadata("Respuesta nok", "-1", "Error al crear Libro" );
			log.error( "Error en crear Libro  " , e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<LibroResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		
		
		return new ResponseEntity<LibroResponseRest>(response,HttpStatus.OK);
	}



	@Override
	@Transactional
	public ResponseEntity<LibroResponseRest> actualizar(Libro Libro, Long id) {
	log.info("Inicio de metodo actualizar() ");
		
		LibroResponseRest response = new LibroResponseRest();
		
		try {
			List<Libro> lista = new ArrayList<>();
			
			Optional<Libro> LibroBuscada = libroDAO.findById(id);
			
			if(LibroBuscada.isPresent())
			{
				LibroBuscada.get().setNombre( Libro.getNombre() );
				LibroBuscada.get().setDescripcion( Libro.getDescripcion() );
				
				Libro LibroActualizar = libroDAO.save( LibroBuscada.get() );
				
				if( LibroActualizar != null)
				{
					lista.add( LibroActualizar );
					response.getLibroResponse().setLibros(lista);
					response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
				}
				else
				{
					log.error( "Error en guardar Libro " );
					response.setMetadata("Respuesta nok", "-1", "Libro NO actualizado");
					return new ResponseEntity<LibroResponseRest>(response,HttpStatus.BAD_REQUEST);
				}

			}
			else
			{
				log.error( "Error en guardar Libro " );
				response.setMetadata("Respuesta nok", "-1", "Libro NO actualizado");
				return new ResponseEntity<LibroResponseRest>(response,HttpStatus.NOT_FOUND);
			}
			
			
			
		 
			

			
			
			
			
		} catch (Exception e) {
			response.setMetadata("Respuesta nok", "-1", "Error al actualizar Libro" );
			log.error( "Error en actualizar Libro  " , e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<LibroResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		
		
		return new ResponseEntity<LibroResponseRest>(response,HttpStatus.OK);
	}



	@Override
	@Transactional
	public ResponseEntity<LibroResponseRest> eliminar(Long id) 
	{
		log.info("Inicio de metodo eliminar() ");
		
		LibroResponseRest response = new LibroResponseRest();
		
		try {
			
			List<Libro> lista = new ArrayList<>();
			
			Optional<Libro> LibroEliminar = libroDAO.findById(id);
			
			if(LibroEliminar.isPresent())
			{
				  libroDAO.deleteById(id);
				 
					lista.add( LibroEliminar.get() );
					response.setMetadata("Respuesta OK", "200", "Libro Eliminado" );
					response.getLibroResponse().setLibros(lista);
			 
			}
			else
			{
				log.error( "Error en guardar Libro " );
				response.setMetadata("Respuesta nok", "-1", "Libro NO encontrado, no eliminado");
				return new ResponseEntity<LibroResponseRest>(response,HttpStatus.NOT_FOUND);
			} 
			
			
		} catch (Exception e) {
			response.setMetadata("Respuesta nok", "-1", "Error al eliminar Libro" );
			log.error( "Error en eliminar Libro  " , e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<LibroResponseRest>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		
		
		return new ResponseEntity<LibroResponseRest>(response,HttpStatus.OK);
	}
	
 

}
