package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class ApiBooksApplication implements CommandLineRunner {
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(ApiBooksApplication.class, args);
	   System.out.println("\n Inicia BOOK APP . . . \n");
	}

	@Override
	public void run(String... args) throws Exception {
	
		String password = "springboot";
		
		for (int i = 0; i < 3; i++) 
		{
			String passworBcrypt = passwordEncoder.encode(password);
			System.out.println(passworBcrypt);
		}

	}

}
