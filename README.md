# Código del curso Spring Boot 2
# Instructor
> [Alfredo Leal](https://www.udemy.com/course/spring-boot-2-desarrollo-de-aplicaciones-backend/) 
 

# Udemy
* Spring Boot 2: Desarrollo de aplicaciones backend


## Notas
 * Angular - framework opensource para front end, creado por google,
 * Levantar el servicio en V SC en terminal colocar ng server -o
  C:\Users\88464\OneDrive - GS\UDEMY\SpringBoot_Ude_2\book-web\clienteFront> ng serve -o
  se levanta en http://localhost:4200/

 * Crear un nuevo elemento -> ng g c components/navbar

~~~
* Repositorio solo en ** Gitlab  **
* TDD -> Test Driven Development  
 ** Escribe una prueba que falle
 ** Hacer el codigo que pase la prueba
 ** Eliminar redundancia


---
## Código 

`//desarrollo` 
 `public int suma( int x, int y ) { return x + y; }` 

 `//test` 
`@Test`
`public int suma( int x, int y ) {  assertEquals(2, ( x + y ) ); }`


 

---
#### Version  V.0.1 
* **V.0.1**  _>>>_  Iniciando proyecto [  on 18 FEB, 2022 ]  
 
 